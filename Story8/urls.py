from django.urls import path
from . import views

app_name= 'Story8'
urlpatterns = [
    path('', views.books, name='books'),
    path('data/bookTitle=<str:bookTitle>', views.data, name='data'),

]