from django.shortcuts import render
from django.http import JsonResponse
import json
import requests

def books(request):
    return render(request, 'books.html')

def data(request, bookTitle="harry potter"):
    link = "https://www.googleapis.com/books/v1/volumes?q=" + bookTitle
    print(link)
    final = requests.get(link).json()
    return JsonResponse(final)
