from django.test import TestCase, Client
from django.urls import resolve
from Story8.views import *


class Story8Test(TestCase):
    def test_story8_url_is_exist(self):
        # Check if books page returns 200
        response = Client().get('/books/')
        self.assertEqual(response.status_code,200)
    
    def test_story8_json_data_url_exists(self):
        # Check if json data page returns 200
        response = Client().get('/books/data/bookTitle=<str:bookTitle>')
        self.assertEqual(response.status_code, 200)

    def test_story8_using_books_func(self):
    	# Check if views is using 'books' function
    	found = resolve('/books/')
    	self.assertEqual(found.func, books)
    
    def test_story8_using_data_func(self):
        # Check if views is using 'data' function
        found = resolve('/books/data/bookTitle=<str:bookTitle>')
        self.assertEqual(found.func, data)
    
    def test_story8_using_books_template(self):
        # Check if books page is using 'books.html' template
        response = Client().get('/books/')
        self.assertTemplateUsed(response, 'books.html')

    def test_story8_contains_greeting(self):
        # Check if greeting is in page
        response = Client().get('/books/')
        response_content = response.content.decode('utf-8')
        self.assertIn("Find Any Books Here!", response_content)
