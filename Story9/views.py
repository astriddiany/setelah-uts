from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import login,logout
from django.contrib.auth.decorators import login_required

@login_required(login_url = "/log/login/")
def index(request):
    return render(request,'welcome.html')

def signup(request):
    if request.method == "POST":
        formSignUp = UserCreationForm(request.POST)
        if formSignUp.is_valid():
            user = formSignUp.save()
            login(request,user)  # log user in
            return redirect('/log/')
    else:
        formSignUp = UserCreationForm()
    return render(request, 'signup.html', {'formSignUp' : formSignUp})

def loginView(request):
    if request.method == "POST":
        formLogIn = AuthenticationForm(data = request.POST)
        if formLogIn.is_valid():
            # log in user
            if 'next' in request.POST:
                user = formLogIn.get_user()
                login(request, user)
                return redirect(request.POST.get("next"))
            else:
                return redirect('/log/')
    else:
        formLogIn = AuthenticationForm()
    return render(request, 'signin.html', {'formLogIn' : formLogIn})

def logoutView(request):
    if request.method == "POST":
        logout(request)
        return redirect('/log/')
