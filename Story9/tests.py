from django.test import TestCase, Client
from django.urls import resolve, reverse
from Story9.views import *
from django.contrib.auth.models import User
from django.http import HttpRequest

class Story9Test(TestCase):
    def test_story9_page_login(self):
        # Check if login page returns 301
        response = Client().get('/log/login')
        self.assertEqual(response.status_code,301)

    def test_story9_page_signup(self):
        # Check if signup page returns 200
        response = Client().get('/log/signup')
        self.assertEqual(response.status_code,301)

    def test_story9_page_logout(self):
        # Check if logot page returns 200
        response = Client().get('/log/logout')
        self.assertEqual(response.status_code,301)
    
    def test_story9_login_using_template(self):
        # Check if login page use signin.html
        response = Client().get('/log/login/')
        self.assertTemplateUsed(response, 'signin.html')

    def test_story9_signup_using_template(self):
        # Check if signup page use signup.html
        response = Client().get('/log/signup/')
        self.assertTemplateUsed(response, 'signup.html')

    def test_story9_using_signup_func(self):
        # Check if views use signup func
        found = resolve('/log/signup/')
        self.assertEqual(found.func, signup)

    def test_story9_using_login_func(self):
        # Check if views use loginView func
        found = resolve('/log/login/')
        self.assertEqual(found.func, loginView)

    def test_story9_login_redirect(self):
        # Check if user login, page log return 200
        user = User.objects.create_user('john', 'lennon@thebeatles.com', 'johnpassword')
        self.client.login(username='john', password='johnpassword')
        response = self.client.post('/log/')
        self.assertEqual(response.status_code, 200)

    def test_story9_logout_redirect(self):
        # Check if user logout, page logout return 200
       user = User.objects.create_user('john', 'lennon@thebeatles.com', 'johnpassword')
       self.client.login(username='john', password='johnpassword')
       response = self.client.post('/log/logout/')
       self.assertEqual(response.status_code, 302)
        

    



