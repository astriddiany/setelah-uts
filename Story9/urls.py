from django.urls import path
from . import views

urlpatterns = [
    path('',views.index),
    path('signup/', views.signup),
    path('login/', views.loginView),
    path('logout/', views.logoutView)
]
