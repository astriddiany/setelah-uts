from django.test import TestCase, Client, override_settings
from django.apps import apps
from django.urls import reverse, resolve
from .views import *

class story7TestEverythings(TestCase):
    def test_accordion_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_url_using_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_template_used(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'accordion.html')
    
    def test_check_template(self):
        response = Client().get('/')
        content = response.content.decode('utf8')
        self.assertIn('Accordion Things', content)
