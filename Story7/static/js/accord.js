/* Story 7 */
$(document).ready(() => {
    $(".accordion-bar").on('click', event => {
        $(event.currentTarget).next().slideToggle();
        const $other_accordions = $(event.currentTarget).parent().siblings();
        $other_accordions.each((index, $accordion) => {
            $($accordion).find('.content').slideUp();
        });
    });

    $('.down').on('click', event => {
        const $accordion = $(event.currentTarget).parents('.accordion');
        $($accordion).before($($accordion).next());
    });

    $('.up').on('click', event => {
        const $accordion = $(event.currentTarget).parents('.accordion');
        $($accordion).after($($accordion).prev());
    });
});


    /* Story 8 */
    if (window.location.href.match(/books/)) {
        find("harry potter");
        $("#book_input").on("keyup", delay(function(e) {
            bookTitle = e.currentTarget.value.toLowerCase()
            find(bookTitle)
        }, 500));
    }


var currentPage;
var title;

/* Delay settings */
function delay(fn, ms) {
    let timer = 0
    return function(...args) {
      clearTimeout(timer)
      timer = setTimeout(fn.bind(this, ...args), ms || 0)
    }
  }

/* Finding book title */
function find(bookTitle){
$.ajax({
    url: "data/bookTitle=" + bookTitle,
    datatype: 'json',
    success: function(data){
        title = bookTitle;
        pageStatus();
        $('tbody').empty();
        var result ='<tr>';

        if (data.totalItems == 0) {
            alert("Buku tidak ditemukan");
            window.location = '/books/';
        } else {
        for(var i = 0; i < data.items.length; i++) {
            try{
                result += "<tr> <th scope='row' class='align-middle text-center'>" + (i+1) + "</th>" +
                "<td><img class='img-fluid' style='width:22vh' src='" 
                + data.items[i].volumeInfo.imageLinks.smallThumbnail +"'></img>" + "</td>" +
                "<td class='align-middle'>" + data.items[i].volumeInfo.title +"</td>" +
                "<td class='align-middle'>" + data.items[i].volumeInfo.authors + "</td>" + 
                "<td class='align-middle'>" + data.items[i].volumeInfo.publisher +"</td>" + 
                "<td class='align-middle'>" + data.items[i].volumeInfo.publishedDate +"</td>" + "</td></tr>";
            }
            catch{

            }
        }
        $('tbody').append(result);
        }
    },

});

}

/* For search button */
function search() {
var word = $("#book_input").val();
find(word);
}

/* Show result */
function pageStatus(){
$('#current')[0].innerHTML = currentPage;
}

